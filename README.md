# OpenML dataset: baseball-team

https://www.openml.org/d/515

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Lorraine Denby    
**Source**: [StatLib](http://lib.stat.cmu.edu/datasets/)  
**Please cite**:   

**Analysis of Baseball Salary Data: Team statistics**  
This analysis describes and summarizes the relationships between 1987 salaries of major league baseball players and the player's performance. The salary data were taken from Sports Illustrated, April 20, 1987. The salary of any player not included in that article is listed as an NA. The 1986 and career statistics were taken from The 1987 Baseball Encyclopedia Update published by Collier  Books,  Macmillan  Publishing  Company, New York. The team attendance figures were obtained from the Elias Sports Bureau, personal conversation.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/515) of an [OpenML dataset](https://www.openml.org/d/515). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/515/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/515/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/515/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

